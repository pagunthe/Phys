#ifndef FLAVOURTAGCLONER_H
#define FLAVOURTAGCLONER_H 1

#include "ObjectClonerBase.h"

#include "MicroDST/ICloneFlavourTag.h"

namespace LHCb
{
  class FlavourTag;
}

/** @class FlavourTagCloner FlavourTagCloner.h
 *
 *  Clones flavour tagging objects
 *
 *  @author Juan PALACIOS
 *  @date   2008-08-08
 */
class FlavourTagCloner : public extends<ObjectClonerBase,ICloneFlavourTag>
{

public:

  /// Standard constructor
  using extends::extends;
  
  LHCb::FlavourTag* operator() (const LHCb::FlavourTag* tag) override;

private:

  typedef MicroDST::BasicCopy<LHCb::FlavourTag> BasicFTCopy;

  LHCb::FlavourTag* clone(const LHCb::FlavourTag* tag);

};

#endif // FLAVOURTAGCLONER_H
