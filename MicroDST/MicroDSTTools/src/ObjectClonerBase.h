#pragma once

// STL
#include <algorithm>

#include <MicroDST/MicroDSTTool.h>
#include <MicroDST/Functors.hpp>

#include "CaloUtils/CaloParticle.h"

/** @class ObjectClonerBase ObjectClonerBase.h
 *
 *  Base class for cloning tools
 *
 *  @author Chris Jones
 *  @date   2012-03-30
 */
class ObjectClonerBase : public MicroDSTTool
{

public:

  /// Standard constructor
  ObjectClonerBase( const std::string& type,
                    const std::string& name,
                    const IInterface* parent);

  /// Initialisation
  StatusCode initialize() override;

protected:

  /// Method to check if a Particle is 'pure neutral calo'
  inline bool isPureNeutralCalo( const LHCb::Particle* P ) const
  {
    return ( P && P->isBasicParticle() &&
             LHCb::CaloParticle((LHCb::Particle*)P).isPureNeutralCalo() );
  }

  /// Method to check if a Particle is charged
  inline bool isChargedBasic( const LHCb::Particle* P ) const
  {
    return ( P && P->isBasicParticle() && P->proto() && P->proto()->track() );
  }

  /// Get TES location for an object
  template<class TYPE>
  std::string tesLocation( const TYPE obj ) const
  {
    return ( obj && obj->parent() && obj->parent()->registry() ?
             obj->parent()->registry()->identifier() : "NotInTES" );
  }

  /// Check to see if a given object should be cloned, or is VETO'ed
  template<class TYPE>
  bool isVetoed( const TYPE obj ) const
  {
    const bool veto =
      ( obj && obj->parent() &&
        !m_tesVetoList.empty() &&
        m_tesVetoList.end() != std::find( m_tesVetoList.begin(),
                                          m_tesVetoList.end(),
                                          tesLocation(obj) ) );
    if ( msgLevel(MSG::DEBUG) )
    {
      if ( veto )
      {
        debug() << "Object in '" << tesLocation(obj)
                << "' is VETO'ed from cloning. Returning original pointer." << endmsg;
      }
      else
      {
        debug() << "Object in '" << tesLocation(obj)
                << "' is NOT VETO'ed from cloning. Returning clone." << endmsg;
      }
    }
    return veto;
  }

  /// Check to see if a given object should always be cloned
  template<class TYPE>
  bool alwaysClone( const TYPE obj ) const
  {
    const bool clone =
      ( obj && obj->parent() &&
        !m_tesAlwaysClone.empty() &&
        m_tesAlwaysClone.end() != std::find( m_tesAlwaysClone.begin(),
                                             m_tesAlwaysClone.end(),
                                             tesLocation(obj) ) );
    if ( msgLevel(MSG::DEBUG) )
    {
      if ( clone )
      {
        debug() << "Object in '" << tesLocation(obj)
                << "' is ALWAYS Cloned." << endmsg;
      }
      else
      {
        debug() << "Object in '" << tesLocation(obj)
                << "' is NOT ALWAYS Cloned." << endmsg;
      }
    }
    return clone;
  }

private:

  /// List of veto'ed TES locations. Original smartrefs kept.
  std::vector<std::string> m_tesVetoList;

  /// List to locations to always clone
  std::vector<std::string> m_tesAlwaysClone;

};
