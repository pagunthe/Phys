// Include files

// LHCb
#include "Event/FlavourTag.h"

// local
#include "FlavourTagCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FlavourTagCloner
//
// 2008-08-08 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================

LHCb::FlavourTag* FlavourTagCloner::operator() (const LHCb::FlavourTag* tag)
{
  return this->clone(tag);
}

//=============================================================================

LHCb::FlavourTag* FlavourTagCloner::clone( const LHCb::FlavourTag* tag )
{
  // Clone the FT object
  LHCb::FlavourTag * tmp = cloneKeyedContainerItem<BasicFTCopy>(tag);

  // Update the Particle SmartRef
  tmp->setTaggedB( getStoredClone<LHCb::Particle>( tag->taggedB() ) );

  // Clear the taggers vector
  tmp->setTaggers( std::vector<LHCb::Tagger>() );

  // return
  return tmp;
}
//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( FlavourTagCloner )
