#include "CaloDigitCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloDigitCloner
//
// 2017-06-15 : Ricardo Vazquez Gomez
//-----------------------------------------------------------------------------

//=============================================================================

CaloDigitCloner::CaloDigitCloner( const std::string& type,
                                  const std::string& name,
                                  const IInterface* parent )
  : base_class ( type, name , parent )
{
  declareProperty( "CreateADCs", m_createADCs = true );
  declareProperty( "EcalADCLocation", m_ecalADCLocation = LHCb::CaloAdcLocation::Ecal );
  declareProperty( "PrsADCLocation", m_prsADCLocation = LHCb::CaloAdcLocation::Prs );
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode CaloDigitCloner::initialize()
{
  const StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  m_edata = tool<ICaloDataProvider>("CaloDataProvider","EcalDataProvider",this);
  m_pdata = tool<ICaloDataProvider>("CaloDataProvider","PrsDataProvider",this);

  return sc;
}

//=============================================================================

LHCb::CaloDigit*
CaloDigitCloner::operator() (const LHCb::CaloDigit* digit)
{
  return this->clone(digit);
}

//=============================================================================

LHCb::CaloDigit* CaloDigitCloner::clone(const LHCb::CaloDigit* digit)
{
  if ( !digit )
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "CaloDigit pointer is NULL !" << endmsg;
    return nullptr;
  }

  if ( !digit->parent() )
  {
    this->Warning( "Cannot clone a CaloDigit with no parent !" ).ignore();
    return nullptr;
  }

  // Is this object in the veto list ?
  if ( isVetoed(digit) ) { return const_cast<LHCb::CaloDigit*>(digit); }

  auto clone = cloneKeyedContainerItem<BasicCaloDigitCloner>(digit);
  if ( !clone ) return clone;

  // make the ADC object
  if ( m_createADCs && !m_spd(digit) )
  {
    const auto tesInputADC =
      ( m_ecal(digit) ? m_ecalADCLocation :
        m_prs(digit)  ? m_prsADCLocation :
        "" );
    if ( !tesInputADC.empty() ) {
      if ( msgLevel(MSG::VERBOSE) ) verbose() << "Looking for existing CaloAdc" << endmsg;
      const auto inputAdcs = getOrCreate<LHCb::CaloAdcs,LHCb::CaloAdcs>(tesInputADC);

      const auto id = digit->cellID();
      // Default values, in case the ADCs need to be created but cannot be
      // decoded from a raw bank (e.g. if the raw bank is missing)
      int adc = (m_ecal(digit)) ? -255 : 0;
      if ( !inputAdcs->object(id) ) {
        adc = m_ecal(digit) ? m_edata->adc( id , adc ) : m_pdata->adc( id , adc );
        inputAdcs->insert( new LHCb::CaloAdc( id, adc ), id );
        if ( msgLevel(MSG::VERBOSE) ) verbose() << "Existing CaloAdc not found, created new: " << adc << endmsg;
      } else {
        adc = inputAdcs->object(id)->adc();
        if ( msgLevel(MSG::VERBOSE) ) verbose() << "Found existing CaloAdc: " << adc << endmsg;
      }

      const auto tesOutputADC = outputTESLocation(tesInputADC);
      const auto outputAdcs = getOrCreate<LHCb::CaloAdcs,LHCb::CaloAdcs>(tesOutputADC);
      if ( !outputAdcs->object(id) ) {
          if ( msgLevel(MSG::VERBOSE) ) verbose() << "Adding new CaloAdc to output: " << adc << endmsg;
          outputAdcs->insert( new LHCb::CaloAdc( id, adc ), id );
      }
    }
  }

  return clone;
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloDigitCloner )

//=============================================================================
