#pragma once

#include "ObjectClonerBase.h"

#include <MicroDST/ICloneCaloDigit.h>            // Interface
#include <MicroDST/Functors.hpp>

// from LHCb
#include "Event/CaloDigit.h"
#include "Event/CaloAdc.h"

#include "CaloUtils/CaloParticle.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloDAQ/ICaloDataProvider.h"

namespace StoreCaloUtils
{
  class DigitFromCalo
  {
  public:
    explicit DigitFromCalo( const int  calo ): m_calo( calo ) {}
    explicit DigitFromCalo( const std::string& calo )
      : DigitFromCalo( CaloCellCode::CaloNumFromName( calo ) ) {}
    inline bool operator() ( const LHCb::CaloDigit* digit ) const
    {
      return digit && ( ( (int) digit->cellID().calo() ) == m_calo );
    }
    DigitFromCalo() = delete;
  private:
    int m_calo{0} ;
  };
}

/** @class CaloDigitCloner CaloDigitCloner.h src/CaloDigitCloner.h
 *
 *  Clone an LHCb::CaloDigit.
 *
 *  @author Ricardo Vazquez Gomez
 *  @date   2017-06-15
 */

class CaloDigitCloner : public extends<ObjectClonerBase,ICloneCaloDigit>
{

public:

  /// Standard constructor
  CaloDigitCloner( const std::string& type,
                   const std::string& name,
                   const IInterface* parent);

  StatusCode initialize() override;

  LHCb::CaloDigit* operator() (const LHCb::CaloDigit* digit) override;

private:

  LHCb::CaloDigit* clone(const LHCb::CaloDigit* digit);

private:

  typedef MicroDST::BasicItemCloner<LHCb::CaloDigit> BasicCaloDigitCloner;

private:

  bool m_createADCs;

  // calodigit checkers
  StoreCaloUtils::DigitFromCalo m_spd{DeCalorimeterLocation::Spd};
  StoreCaloUtils::DigitFromCalo m_prs{DeCalorimeterLocation::Prs};
  StoreCaloUtils::DigitFromCalo m_ecal{DeCalorimeterLocation::Ecal};

  /// ECAL adc provider
  ICaloDataProvider* m_edata = nullptr;

  /// PRS adc provider
  ICaloDataProvider* m_pdata = nullptr;


  /// Location of input CaloAdc container for ECAL ADCs
  std::string m_ecalADCLocation;

  /// Location of input CaloAdc container for PRS ADCs
  std::string m_prsADCLocation;

};
