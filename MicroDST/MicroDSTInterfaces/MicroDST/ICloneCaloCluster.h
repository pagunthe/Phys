
#pragma once

// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb
{
  class CaloCluster;
  class Particle;
}

/** @class ICloneCaloCluster MicroDST/ICloneCaloCluster.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2010-08-13
 */
class GAUDI_API ICloneCaloCluster : virtual public MicroDST::ICloner<LHCb::CaloCluster>
{

public:
  
  /// Interface ID
  DeclareInterfaceID(ICloneCaloCluster, 1, 0 );
  
  /// Destructor
  virtual ~ICloneCaloCluster() { }
  
public:
  
  /// clone a cluster with parent info
  virtual LHCb::CaloCluster* clone( const LHCb::CaloCluster* hypo,
                                    const LHCb::Particle * parent = nullptr ) = 0;

};
