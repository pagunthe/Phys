// $Id: CopyParticle2PVRelations.cpp,v 1.1 2009-04-15 20:30:23 jpalac Exp $
// Include files 
#include "CopyParticle2PVRelations.h"

DECLARE_COMPONENT_WITH_ID( CopyParticle2PVRelations, "CopyParticle2PVRelations" )

const std::string Defaults<Particle2Vertex::Table>::clonerType = "VertexBaseFromRecVertexCloner";
const std::string Location<Particle2Vertex::Table>::Default = "NO DEFAULT LOCATION";
