// Include files

// from Gaudi
#include <GaudiKernel/DataObject.h>
#include <GaudiKernel/RegistryEntry.h>
// local
#include "MoveDataObject.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MoveDataObject
//
// 2010-09-28 : Juan Palacios
//-----------------------------------------------------------------------------
//=============================================================================
// Main execution
//=============================================================================
StatusCode MoveDataObject::execute()
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

    for ( const auto & loc : this->inputTESLocations() )
    {
      executeLocation( niceLocationName(loc) );
    }

  return StatusCode::SUCCESS;
}

//=============================================================================
void MoveDataObject::executeLocation(const std::string& objectLocation)
{

  std::string outputLocation = m_outputTESLocation;
  if ( outputLocation.empty() )
    outputLocation = this->outputTESLocation( objectLocation );

  if ( msgLevel(MSG::VERBOSE) )
    verbose() << "Going to move DataObject from " << objectLocation
              << " to " << outputLocation << endmsg;

  using GenericContainer = KeyedContainer < KeyedObject<int> , Containers::HashMap >;

  DataObject * doObj = nullptr;
  
  auto sc = eventSvc()->retrieveObject(objectLocation,doObj);
  auto pObj = dynamic_cast<ObjectContainerBase*>(doObj);

  if (sc.isFailure() )
  {
    Error("Failed to retrieve DataObject from "+objectLocation,
          StatusCode::FAILURE, 0).ignore();
    return;
  }

  doObj = nullptr;
  sc = eventSvc()->findObject(outputLocation,doObj);
  auto targetObj = dynamic_cast<GenericContainer*>(doObj);
  
  const bool emptyDestination = !sc.isSuccess();
  
  if ( pObj )
  {
    sc = eventSvc()->unregisterObject(pObj);
    if ( !sc.isSuccess() )
    {
      Error("Failed to unregister DataObject from "+objectLocation,
            StatusCode::FAILURE, 0).ignore();

      return;
    }

    if ( outputLocation == "KILL" )
    {
      verbose() << "Killed location " << objectLocation << endmsg;
      return ;
    }

    if ( emptyDestination )
    {
      sc = eventSvc()->registerObject(outputLocation, pObj);
      if ( sc.isFailure() )
      {
        Error("Could not register the new object in " + outputLocation,
                StatusCode::FAILURE, 0).ignore();
      }
    }
    else if ( targetObj )
    {
      Warning("Non-empty destination while copying "
            + objectLocation + " "
            + m_outputTESLocation
              ).ignore();
      
      auto pObjAsKeyed = dynamic_cast<GenericContainer*>(pObj);
      if (pObjAsKeyed)
      {
        std::vector<int> existingKeys;
        existingKeys.reserve( targetObj->size() );
        for ( const auto obj : *targetObj )
        {
          existingKeys . push_back ( obj->key() ) ;
        }
        for ( const auto obj : *pObjAsKeyed )
        {
          const auto key = obj->key();
          for ( const auto eKey : existingKeys )
          { 
            if ( key == eKey )
            {
              Error ( "Trying to merge KeyedContainers including "
                      "objects with the same key.",  StatusCode::FAILURE, 0).ignore();
              return;
            }
          }
          
          obj->setParent ( targetObj );
          targetObj->insert ( obj );
        }

        sc = eventSvc()->updateObject ( targetObj );
        if ( sc.isFailure() )
        {
          Error("Failed to update the content of " + outputLocation,
                StatusCode::FAILURE, 0).ignore();
        }
      }
      else
      {
        return;
      }
    }
    else
    {
      if ( emptyDestination )
      {
        Error("Failed to move the object from "+
              objectLocation+ " to " + outputLocation,
              StatusCode::FAILURE, 0).ignore();
      }
      else
      {
        Error("Failed to merge objects from "+
              objectLocation+ " and " + outputLocation,
              StatusCode::FAILURE, 0).ignore();
      }
      
      return;
    }

    pObj->release();
  }

}
//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MoveDataObject )
