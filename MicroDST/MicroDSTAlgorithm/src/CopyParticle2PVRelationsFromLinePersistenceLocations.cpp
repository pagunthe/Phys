#include <string>
#include <vector>

#include "CopyParticle2PVRelationsFromLinePersistenceLocations.h"

DECLARE_COMPONENT( CopyParticle2PVRelationsFromLinePersistenceLocations )

CopyParticle2PVRelationsFromLinePersistenceLocations::CopyParticle2PVRelationsFromLinePersistenceLocations(const std::string& name, ISvcLocator* svcLocator)
  : CopyParticle2PVRelations(name, svcLocator) {
  m_linesToCopy.declareUpdateHandler(
    [this](Property&) {
      this->m_linesToCopySet = std::set<std::string>(m_linesToCopy.begin(), m_linesToCopy.end());
    }
  );
  m_linesToCopy.useUpdateHandler();
}

StatusCode CopyParticle2PVRelationsFromLinePersistenceLocations::initialize() {
  StatusCode sc = CopyParticle2PVRelations::initialize();
  if (sc.isFailure()) {
    return sc;
  }

  m_linePersistenceSvc = svc<ILinePersistenceSvc>(m_linePersistenceSvcName.value());
  if (!m_linePersistenceSvc) {
    throw GaudiException("Could not acquire ILinePersistenceSvc", this->name(), StatusCode::FAILURE);
  }

  return StatusCode::SUCCESS;
}

StatusCode CopyParticle2PVRelationsFromLinePersistenceLocations::execute() {
  if (UNLIKELY(msgLevel(MSG::DEBUG))) {
    debug() << "==> Execute" << endmsg;
  }
  setFilterPassed(true);

  const auto decReports = getIfExists<LHCb::HltDecReports>(m_hltDecReportsLocation.value());
  if (!decReports) {
    return Warning("Could not retrieve HltDecReports from " + m_hltDecReportsLocation.value(),
                   StatusCode::SUCCESS);
  }
  const auto locationsToCopy = m_linePersistenceSvc->locationsToPersist(*decReports, m_linesToCopySet);

  if (msgLevel(MSG::VERBOSE)) {
    if (locationsToCopy.empty()) {
      verbose() << "No locations to copy" << endmsg;
    } else {
      verbose() << "Will attempt to copy the following locations:" << endmsg;
    }
    for (const auto& loc : locationsToCopy) {
      verbose() << "  - " << loc << endmsg;
    }
  }

  for (const auto& location: locationsToCopy) {
    this->copyTableFromLocation(location);
  }

  return StatusCode::SUCCESS;
}
