#ifndef COPYCALOADCS_H
#define COPYCALOADCS_H 1

// Include files
// from MicroDST
#include <MicroDST/KeyedContainerClonerAlg.h>
#include <MicroDST/ICloneCaloAdc.h>
#include "MicroDST/BindType2ClonerDef.h"
// from LHCb
#include <Event/CaloAdc.h>

/** @class CopyCaloAdcs CopyCaloAdcs.h
 *
 * MicroDSTAlgorithm to clone LHCb::CaloAdc and related objects from one TES
 * location to a parallel one.
 * It inherits the std::string properties InputLocation and OutputPrefix from
 * MicroDSTCommon.
 * The LHCb::CaloAdc objects are taken from the TES location defined by
 * InputLocation, and are cloned and put in TES location "/Event" +
 * OutputPrefix + InputLocation.
 * If InputLocation already contains a leading "/Event" it is removed.
 * The actual cloning of individual LHCb::CaloAdc objects is performed by the
 * ICloneCaloAdc tool, the implementation of which is set by the
 * ClonerType property (default: CaloAdcCloner).
 *
 * Usually, one would clone LHCb::CaloAdc objects implicitly by using
 * CopyProtoParticles, which copies the associated LHCb::CaloAdc objects.
 * The CopyCaloAdcs algorithm if useful for when one wants to clone objects
 * that one knows may not be associated to LHCb::ProtoParticles objects, but
 * which one wants to copy anyway.
 *
 * @see ICloneCaloAdc
 * @see CaloAdcCloner
 *
 * <b>Example</b>: Clone CaloAdcs from "/Event/Hlt/Calo/EcalAdcs" to 
 * "/Event/MyLocation/Hlt/Calo/EcalAdcs"
 *
 *  @code
 *  copyAdcs = CopyCaloAdcs()
 *  copyAdcs.OutputPrefix = "MyLocation"
 *  copyAdcs.InputLocation = "Hlt/Calo/EcalAdcs"
 *  // Add the CopyCaloAdcs instance to a selection sequence
 *  mySeq = GaudiSequencer("SomeSequence")
 *  mySeq.Members += [copyAdcs]
 *  @endcode
 */
template <> struct BindType2Cloner<LHCb::CaloAdc> 
{
  typedef LHCb::CaloAdc Type;
  typedef ICloneCaloAdc Cloner;
};
//=============================================================================
template<> struct Defaults<LHCb::CaloAdc>
{
  const static std::string clonerType;
};
const std::string Defaults<LHCb::CaloAdc>::clonerType = "CaloAdcCloner";
//=============================================================================
template<> struct Location<LHCb::CaloAdc>
{
  const static std::string Default;
};
const std::string Location<LHCb::CaloAdc>::Default = "";
//=============================================================================
typedef MicroDST::KeyedContainerClonerAlg<LHCb::CaloAdc> CopyCaloAdcs;
DECLARE_COMPONENT_WITH_ID( CopyCaloAdcs, "CopyCaloAdcs" )
#endif // COPYCALOADCS_H
