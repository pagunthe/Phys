__version__ = "$Id: HltJetConf.py,v 1.0 2017-03-06$"
__author__  = "Stephen Farry <stephen.farry@cern.ch>"

from LHCbKernel.Configuration import *

from Configurables import ( GaudiSequencer, CellularAutomatonAlg, CaloClusterCovarianceAlg, CaloPhotonMatch , PhotonMatchAlg, ChargedProtoParticleMaker, DelegatingTrackSelector, TrackSelector)

from CommonParticles import (StdLooseAllPhotons, StdLooseResolvedPi0, StdLooseMergedPi0, StdLooseKs, StdLooseLambda)


from Configurables import HltParticleFlow
class HltParticleFlowConf:
    def __init__(self, _name, _inputs = ['Photons', 'ResolvedPi0s', 'MergedPi0s', 'Ks', 'Lambda', 'ChargedProtos', 'NeutralProtos', 'EcalClusters', 'HcalClusters', 'EcalMatch', 'HcalMatch', 'PrimaryVtxs' ], **kwargs):
        self.name = _name
        #default parameters
        self.params = {
            'ProBestNames': ['mu+', 'e+', 'p+', 'K+', 'pi+'],
            'ProBestKeys' : [ 701, 700, 704, 703, 702],
            'ProBestMins' : [ 0.5 , 0.5 , 0.5 , 0.5, 0.5 ],
            'SprPid'      : -22,
            'EcalBest'    : True,
            'SprRecover'  : False,
            'TrkLnErrMax' : 10,
            'TrkUpErrMax' : 10,
            'TrkDnErrMax' : 10
            }

        # update with user settings
        self.params.update(**kwargs)

         #possible inputs
        self.prepared_inputs = {
            'Photons'       : ['Particle',       'particle', StdLooseAllPhotons],
            'ResolvedPi0s'  : ['Particle',       'particle', StdLooseResolvedPi0],
            'MergedPi0s'    : ['Particle',       'particle', StdLooseMergedPi0],
            'Ks'            : ['Particle',       'particle', StdLooseKs],
            'Lambda'        : ['Particle',       'particle', StdLooseLambda],
            'ChargedProtos' : ['ProtoParticle',  'best'    , 'Rec/ProtoP/Charged'],
            'NeutralProtos' : ['ProtoParticle',  'gamma'   , 'Rec/ProtoP/Neutrals'],
            'EcalClusters'  : ['CaloCluster',    'gamma'   , 'Rec/Calo/EcalClusters'],
            'HcalClusters'  : ['CaloCluster',    'pi0'     ,  self._hcalclusters],
            'EcalMatch'     : ['IClusTrTable2D', 'ecal'    , 'Rec/Calo/ClusterMatch'],
            'HcalMatch'     : ['IClusTrTable2D', 'hcal'    ,  self._hcaltrackmatch],
            'VeloProtos'    : ['ProtoParticle' , 'best'    ,  self._veloprotos],
            'PrimaryVtxs'   : ['RecVertex',      'vertex'  , 'Rec/Vertex/Primary']
            }
        #inputs selected by user - order is important
        self.inputs = _inputs
        self.seq = GaudiSequencer(_name+"ParticleFlowSeq",IgnoreFilterPassed = True)
        #initialise and configure
        self.pflow = HltParticleFlow(_name , **self.params )
        self.configure()
        
    ## Configure the pflow algorithm
    def configure( self ):
        self.pflow.Output = "Rec/"+self.name+"/Particles"
        for key in self.inputs:
            if key in self.prepared_inputs.keys():
                inputClass, inputType, loc = self.prepared_inputs[key]

                #for hcal cluster or matching call function to add to sequence and return loc
                if key == 'HcalClusters' or key == 'HcalMatch' or key == 'VeloProtos':
                    self.pflow.Inputs.append([inputClass, inputType, loc()])

                #for common particles add all locations found as separate inputs
                elif hasattr(loc, 'locations'):
                    for l in loc.locations:
                        #ignore long-downstream locations
                        if 'LD' not in l:
                            self.pflow.Inputs.append([inputClass, inputType, l])

                #if location given as string just add
                else:
                    self.pflow.Inputs.append([inputClass, inputType, loc])

            elif isinstance(key, list) and len(key) == 3:
                self.pflow.Inputs.append([key[0], key[1], key[2]])
            else:
                print "Cannot add input ",key, " to particle flow"
        self.seq.Members += [ self.pflow ]

    def _hcalclusters(self):
       ## Create Sequencer
        ## Call the cluster creation
        hcalClus = CellularAutomatonAlg('HcalClusterization')  # name is enough to setup I/O
        self.seq.Members += [hcalClus]
        ## Get the covariance matrix
        clustCov = CaloClusterCovarianceAlg('HcalCov')
        clustCov.EnergyTags = [ '2x2' ]
        clustCov.CovarianceParameters["Stochastic"] = [0.7]
        clustCov.CovarianceParameters["GainError"]  = [0.1]
        clustCov.OutputData = 'Rec/Calo/HcalClusters'
        self.seq.Members += [clustCov]
        return clustCov.OutputData


    def _hcaltrackmatch(self):
       ## Create Sequencer
        hcal2Track = PhotonMatchAlg("Hcal2TrackMatching")
        hcal2Track.Calos  = ["Rec/Calo/HcalClusters" ]
        hcal2Track.Output = "Rec/Calo/HcalClusterMatch"
        hcal2Track.Filter = "Rec/Calo/InAccHcal"
        hcal2Track.addTool(CaloPhotonMatch, "HcalMatch" )
        hcal2Track.Tool   = "CaloPhotonMatch/HcalMatch"
        hcal2Track.Threshold = "1000"
        hcal2Track.HcalMatch.Calorimeter =  "/dd/Structure/LHCb/DownstreamRegion/Hcal"
        hcal2Track.HcalMatch.Tolerance= "60"
        hcal2Track.HcalMatch.Extrapolator= "TrackRungeKuttaExtrapolator/Regular"
        self.seq.Members += [hcal2Track]
        return hcal2Track.Output

    def _veloprotos(self):
        protos = ChargedProtoParticleMaker("VeloProtoPMaker")
        protos.Inputs = ["Rec/Track/Best"]
        protos.Output = "Rec/ProtoP/VeloProtoPMaker"
        protos.addTool( DelegatingTrackSelector, name="TrackSelector" )
        protos.TrackSelector.TrackTypes = ["Velo"]
        self.seq.Members+= [ protos ]
        return protos.Output


    def getSeq(self):
        return self.seq

    def getOutputLocation(self):
        return self.pflow.Output

# ----------------------------------------------------------------------------------

from Configurables import HltJetBuilder

class HltJetBuilderConf:

    def __init__(self, _name, _inputs, **kwargs):


        self.params = {
            'JetR' : 0.5 ,
            'JetPtMin' : 5000.,
            'InputBans' : [],
            'InputTags' : [],
            'JetPid' : 98,
            'JetVrt' : False,
            'JetSort' : 2,
            'JetInfo' : True,
            'JetEcPath' : "JEC1405",
            'JetEcShift' : 0.0
            }
        
        self.params.update(**kwargs)

        self.name = _name
        if isinstance(_inputs, str):
            self.inputs = [ _inputs ]
        else:
            self.inputs = _inputs
        self.seq = GaudiSequencer(self.name+'JetBuilderSeq')
        self._configure()

    def _configure(self):
        self.jb = HltJetBuilder(self.name, **self.params)
        self.jb.Inputs = self.inputs
        self.seq.Members += [self.jb]

    def getSeq(self):
        return self.seq
