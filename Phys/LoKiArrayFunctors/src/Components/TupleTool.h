// ============================================================================
#ifndef COMPONENTS_TUPLETOOL_H
#define COMPONENTS_TUPLETOOL_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/VectorMap.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/Tuples.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// DaVinciKernel
// ============================================================================
#include "Kernel/IParticleTupleTool.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/PhysTypes.h"
#include "LoKi/IHybridFactory.h"
// ============================================================================
// Boost
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
// Local
// ============================================================================
#include "Preambulo.h"
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Hybrid
  {
    // ========================================================================
    /** @class TupleTool
     *  The simple "hybrid"-based implementation of
     *  the abstract interface IParticleTupleTool
     *  @see IParticleTupleTool
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-19
     */
    class TupleTool
      : public virtual IParticleTupleTool
      , public                  GaudiTool
    {
    public:
      // ======================================================================
      /** helper class to keep the N-tuple items
       *  it is needed due to absence f the default constructor for
       *  the class LoKi::PhysTypes::Fun
       *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
       *  @date 2008-03-19
       */
      class Item
      {
      public:
        // ====================================================================
        // the default constructor
        Item ()
          : m_name ()
          , m_fun  ( LoKi::BasicFunctors<const LHCb::Particle*>::Constant ( -1.e+10 ) )
        {}
        // ====================================================================
      public:
        // ====================================================================
        double  operator() ( const LHCb::Particle* p ) const
        { return m_fun.fun ( p ) ; }
        // ====================================================================
      public:
        /// the variable name
        std::string           m_name ; // the variable name
        /// the functor
        LoKi::PhysTypes::Fun  m_fun  ; /// the functor
      } ;
      // ======================================================================
    public:
      // ======================================================================
      /** Fill the tuple.
       *  @see IParticleTupelTool
       *  @param top      the top particle of the decay.
       *  @param particle the particle about which some info are filled.
       *  @param head     prefix for the tuple column name.
       *  @param tuple    the tuple to fill
       *  @return status code
       */
      StatusCode fill
      ( const LHCb::Particle* top      ,
        const LHCb::Particle* particle ,
        const std::string&    head     ,
        Tuples::Tuple&        tuple    ) override;
      // ======================================================================
    public:
      // ======================================================================
      /// initialization of the tool
      StatusCode initialize () override;
      // ======================================================================
      /// finalization of the tool
      StatusCode finalize () override;
      // ======================================================================
    protected:
      // ======================================================================
      /// initialization of the tool
      virtual StatusCode initVariables () ;
      // ======================================================================
    public:
      // ======================================================================
      /// the update handler
      void propHandler ( Property& /* p */ )  ;
      // ======================================================================
    public:
      // ======================================================================
      /// the preambulo
      std::string preambulo() const { return _preambulo ( m_preambulo ) ; }
      // ======================================================================
      /// constructor
      TupleTool
      ( const std::string& type   ,
        const std::string& name   ,
        const IInterface*  parent ) ;
      // ======================================================================
    protected:
      // ======================================================================
      /// the actual type of {"name":"functor"} map
      typedef std::map<std::string,std::string>  Map ;
      /// the actual type of containter of items
      typedef std::vector<std::pair<std::string,Item> > Items ;
      // ======================================================================
      /// get the items
      const Items&       items   () const { return m_items   ; }
      /// get the factory
      const std::string& factory () const { return m_factory ; }
      // ======================================================================
    private :
      // ======================================================================
      /// the typename of the hybrid factory
      std::string             m_factory ; // the typename of the hybrid factory
      /// { "name":"functor"} map
      Map                      m_vars       ;        // { "name":"functor"} map
      /// n-tuple columns
      Items                    m_items      ;        //         N-tuple columns
      /// preambulo
      std::vector<std::string> m_preambulo  ;        //               preambulo
      // ======================================================================
    };
    // ========================================================================
  } //                                            end of namespace LoKi::Hybrid
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
#endif // COMPONENTS_TUPLETOOL_H
// ============================================================================
//                                                                      The END
// ============================================================================
