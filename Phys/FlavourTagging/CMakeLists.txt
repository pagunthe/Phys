################################################################################
# Package: FlavourTagging
################################################################################
gaudi_subdir(FlavourTagging)

gaudi_depends_on_subdirs(Calo/CaloInterfaces
                         Event/PhysEvent
                         GaudiPython
                         Phys/DaVinciKernel
                         Phys/LoKiPhys
                         PhysSel/PhysSelPython
                         Tr/TrackInterfaces)

find_package(ROOT COMPONENTS MLP Graf Hist Matrix TreePlayer Gpad Graf3d TMVA)

gaudi_install_headers(FlavourTagging)

find_package(Boost)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(FlavourTagging
                 src/*.cpp
		 src/TMVAClassification/mva_charmtagger_reco14/Histogram.cpp
                 INCLUDE_DIRS Tr/TrackInterfaces
                 LINK_LIBRARIES PhysEvent GaudiPythonLib
                                DaVinciKernelLib LoKiPhysLib ROOT)

set_property(SOURCE src/PionSSWrapper.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -O2 " )
set_property(SOURCE src/ProtonSSWrapper.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -O2 " )

gaudi_add_executable(summaryof
                     summary/summaryof.cpp
                     INCLUDE_DIRS Tr/TrackInterfaces
                     LINK_LIBRARIES PhysEvent GaudiPythonLib
                                    DaVinciKernelLib LoKiPhysLib ROOT)

gaudi_install_python_modules()

gaudi_env(SET FLAVOURTAGGINGOPTS \${FLAVOURTAGGINGROOT}/options)

