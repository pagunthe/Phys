// Include files

// local
#include "PionSSWrapper.h"

#ifndef __clang__
namespace MyPionSSSpace {
#include "TMVAClassification/BDT_SSpion/BDT_SSpion_Reco14.class.C"
} // namespace MyPionSSSpace
#endif


//-----------------------------------------------------------------------------
// Implementation file for class : PionSSWrapper
//
// 2014-02-19 : Jack Timpson Wimberley
//-----------------------------------------------------------------------------

PionSSWrapper::PionSSWrapper(std::vector<std::string> & names) {
#ifdef __clang__
  int size = names.size();
  if (size == 0)
    std::cout << "WARNING: NO VALUES PASSED" << std::endl;
#else
  reader = new MyPionSSSpace::ReadssPion(names);
#endif
}

PionSSWrapper::~PionSSWrapper() {
#ifndef __clang__
  delete reader;
#endif
}

double PionSSWrapper::GetMvaValue(std::vector<double> const & values) {
#ifdef __clang__
  int size = values.size();
  if (size == 0)
    std::cout << "WARNING: NO VALUES PASSED" << std::endl;
  return 0.0;
#else
  return reader->GetMvaValue(values);
#endif
}

//=============================================================================
