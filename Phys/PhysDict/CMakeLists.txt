################################################################################
# Package: PhysDict
################################################################################
gaudi_subdir(PhysDict)

gaudi_depends_on_subdirs(Event/HltEvent
                         Event/RecEvent)

find_package(ROOT)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS})

gaudi_add_dictionary(Phys
                     dict/PhysDict.h
                     dict/PhysDict.xml
                     LINK_LIBRARIES HltEvent RecEvent
                     OPTIONS "-U__MINGW32__")

