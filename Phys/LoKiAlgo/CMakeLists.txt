################################################################################
# Package: LoKiAlgo
################################################################################
gaudi_subdir(LoKiAlgo)

gaudi_depends_on_subdirs(GaudiAlg
                         GaudiPython
                         Phys/DaVinciKernel
                         Phys/LoKiPhys)

find_package(PythonLibs)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                           ${PYTHON_INCLUDE_DIRS})

gaudi_add_library(LoKiAlgo
                  src/*.cpp
                  PUBLIC_HEADERS LoKi
                  INCLUDE_DIRS PythonLibs
                  LINK_LIBRARIES PythonLibs DaVinciKernelLib LoKiPhysLib GaudiAlgLib)

gaudi_add_dictionary(LoKiAlgo
                     dict/LoKiAlgoDict.h
                     dict/LoKiAlgo.xml
                     INCLUDE_DIRS PythonLibs PythonLibs
                     LINK_LIBRARIES PythonLibs PythonLibs DaVinciKernelLib LoKiPhysLib GaudiPythonLib LoKiAlgo
                     OPTIONS " -U__MINGW32__ ")

gaudi_install_python_modules()


gaudi_add_test(QMTest QMTEST)
