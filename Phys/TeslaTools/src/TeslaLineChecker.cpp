// Include files

// local
#include "TeslaLineChecker.h"


#include "GaudiKernel/IIncidentSvc.h"
#include "Event/HltDecReports.h"

//debug
#include "Event/ODIN.h"
#include <boost/regex.hpp>

//-----------------------------------------------------------------------------
// Implementation file for class : TeslaLineChecker
//
// 2016-07-20 : Sascha Stahl
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TeslaLineChecker )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TeslaLineChecker::TeslaLineChecker( const std::string& name,
                                    ISvcLocator* pSvcLocator)
  : base_class ( name , pSvcLocator )
{
  declareProperty( "RequestedLines"     ,  m_requestedLines   = {"Hlt2CharmHadD02HH_D02KKTurbo",
                                                                 "Hlt2CharmHadDsp2KS0KmKpPip_KS0DDTurboDecision"} );
  declareProperty( "IgnoredLines"       ,  m_ignoredLines     = {} );
  declareProperty( "DecReportsLocation" ,  m_decRepLoc        = LHCb::HltDecReportsLocation::Hlt2Default );
  declareProperty( "CheckDuplicates"    ,  m_checkDuplicates  = false );
  declareProperty( "CheckNonTurbo"      ,  m_checkNonTurbo    = false );
}


//=============================================================================
//  Incident handling
//=============================================================================
void TeslaLineChecker::handle(const Incident& incident) {
  // debug
  debug() << incident.type() << " incident received from " << incident.source()
          << endmsg;
  auto odin = get<LHCb::ODIN>( LHCb::ODINLocation::Default );
  debug() << "Run " << odin->runNumber() << endmsg;
  m_execute = true;
}

//=============================================================================
// Initialization
//=============================================================================
class isCopy {
  std::set<std::string> existing;
public:
  bool operator()(std::string const &in) {
    return !existing.insert(in).second;
  }
};

StatusCode TeslaLineChecker::initialize() {
  StatusCode sc = DaVinciHistoAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  m_incSvc = svc<IIncidentSvc>("IncidentSvc", true);
  m_incSvc->addListener(this, "RunChange");

  m_teslaLines = m_requestedLines;

  if (m_checkDuplicates){
    isCopy iscopy;
    auto copies = std::remove_if( m_teslaLines.begin(), m_teslaLines.end(), std::ref(iscopy) );
    if ( copies != m_teslaLines.end() ){
      warning() << "Line duplicates: " << endmsg;
      for (auto s = copies ; s != m_teslaLines.end(); s++){
        warning() << *s << endmsg;
      }
      warning() << "Please fix." << endmsg;
    }
    m_teslaLines.erase( copies, m_teslaLines.end() );
  }
  if (m_checkNonTurbo){
    auto nonTurbo = std::remove_if( m_teslaLines.begin(), m_teslaLines.end(),
                                    [this](const std::string& line) {
                                      return ( line.find("Turbo") == std::string::npos && !ignoreLine( line )) ; }
                                    );
    if ( nonTurbo != m_teslaLines.end() ){
      warning() << "Non Turbo Lines added to configuration: " << endmsg;
      for (auto s = nonTurbo ; s != m_teslaLines.end(); s++){
        warning() << *s << endmsg;
      }
      warning() << "Please fix." << endmsg;
    }
  }

  return StatusCode::SUCCESS;
}

bool TeslaLineChecker::ignoreLine(const std::string& line){
  for (const auto& filter: m_ignoredLines){
    boost::match_results<std::string::const_iterator> matches;
    boost::regex re(filter);
    if( boost::regex_match(line, matches, re, boost::match_default) ) {
      return true;
    }
  }
  return false;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode TeslaLineChecker::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  setFilterPassed(true);  // Mandatory. Set to true if event is accepted.
  if ( !m_execute ){
    return StatusCode::SUCCESS;
  }

  auto odin = get<LHCb::ODIN>( LHCb::ODINLocation::Default );
  if ( ( odin->eventType()&LHCb::ODIN::EventTypes::Lumi ) > 0 && ( odin->eventType()&LHCb::ODIN::EventTypes::Physics ) == 0 ){
    return StatusCode::SUCCESS;
  }

  auto decReports = get<LHCb::HltDecReports>( m_decRepLoc );
  if ( decReports->decReports().size() == 0 ){
    return Error("Hlt2DecReports do not exist at the given location: " + m_decRepLoc, StatusCode::FAILURE, 10);
  }

  std::vector<std::string> decReportsNames;
  for ( auto decReport : decReports->decReports() ){
    // Remove Decision from the name
    auto lineName = decReport.first.substr(0,decReport.first.size()-8);
    if ((decReport.second.executionStage() & 0x80 ) && !ignoreLine( lineName )){
      decReportsNames.emplace_back( lineName );
    }
  }

  std::sort( m_teslaLines.begin(), m_teslaLines.end() );
  std::sort( decReportsNames.begin(), decReportsNames.end() );
  std::vector<std::string> missingLines;
  std::set_difference( decReportsNames.begin(), decReportsNames.end(),
                       m_teslaLines.begin(), m_teslaLines.end(),
                       std::back_inserter( missingLines )
                       );
  if ( !missingLines.empty() ){
    error()<<"Someone made a boo-boo! Missing lines:"<<endmsg;
    for (const auto & missingLine : missingLines ){
      error()<<missingLine<<endmsg;
    }
    error()<<"Affected run : "<< odin->runNumber() << endmsg;
    error()<<"Affected TCK : 0x"<< std::hex << decReports->configuredTCK()  << endmsg;
    error()<<"FIX!"<<endmsg;
    error()<<"In case you are testing, set Tesla().EnableLineChecker = False to skip this check."<<endmsg;
    return StatusCode::FAILURE;
  }

  m_execute = false;
  return StatusCode::SUCCESS;
}
