// $Id: ParticleTisTos.h,v 1.1 2010-07-21 21:22:16 tskwarni Exp $
#ifndef PARTICLETISTOS_H
#define PARTICLETISTOS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IParticleTisTos.h"            // Interface

#include "TisTos/TisTos.h"

#include "CaloInterfaces/ITrack2Calo.h"

#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"
#include "Event/Vertex.h"
#include "Event/Track.h"
#include "Event/HltObjectSummary.h"

/** @class ParticleTisTos ParticleTisTos.h
 *
 *  @author Tomasz Skwarnicki
 *  @date   2010-07-08
 *
 *  Default implementation of IParticleTisTos tool
 */
class ParticleTisTos :  public TisTos,
                        virtual public IParticleTisTos {
public:

  /// Standard constructor
  ParticleTisTos( const std::string& type,
                  const std::string& name,
                  const IInterface* parent);

  virtual ~ParticleTisTos( ); ///< Destructor


  StatusCode         initialize() override;

  // ------------  various ways to define Signal (off-line input) -------------------------

  /// Particle input; for composite particles loop over daughters will be executed (true if Signal changed)
  bool addToSignal( const LHCb::Particle & particle ) override;

  /// Proto-particle input
  bool addToSignal( const LHCb::ProtoParticle & protoParticle ) override;

  /// Track input
  bool addToSignal( const LHCb::Track & track ) override;

  /// Hits input
  bool addToSignal( const std::vector<LHCb::LHCbID> & hits )  override { return addHitsToSignal(hits); }


  // -------------------------------------------------
  // ------------ outputs
  // -------------------------------------------------

  /// completely classify the Trigger object with respect to the previouly defined Signal
  unsigned int tisTos(const LHCb::Particle & particle)  override;
  unsigned int tisTos(const LHCb::RecVertex & recVertex )  override;
  unsigned int tisTos(const LHCb::Vertex & vertex )  override;
  unsigned int tisTos(const LHCb::Track & track ) override;
  unsigned int tisTos(const std::vector<LHCb::LHCbID> & hits, unsigned int & valid)
    { return tisTosSortedHits( sortedHits(hits),valid);  } // output: valid==0 -> no hits, dummy result returned
  unsigned int tisTos(const std::vector<LHCb::LHCbID> & hits ) override { return tisTosSortedHits( sortedHits(hits)); }
  unsigned int tisTos(const LHCb::HltObjectSummary & hos, unsigned int & valid  ); // output: valid==0 -> no hits, dummy result
  unsigned int tisTos(const LHCb::HltObjectSummary & hos ) override { unsigned int valid; return tisTos(hos,valid); }


  /// check for TOS  - may be faster than using tisTos()
  bool tos(const LHCb::Particle & particle) override;
  bool tos(const LHCb::RecVertex & recVertex) override;
  bool tos(const LHCb::Vertex & vertex) override;
  bool tos(const LHCb::Track & track ) override;
  bool tos(const std::vector<LHCb::LHCbID> & hits, unsigned int & valid )
    { return tosSortedHits( sortedHits(hits),valid );  } // output: valid==0 -> no hits, dummy result returned
  bool tos(const std::vector<LHCb::LHCbID> & hits ) override {    return tosSortedHits( sortedHits(hits) );  }
  bool tos(const LHCb::HltObjectSummary & hos, unsigned int & valid ); // output: valid==0 -> no hits, dummy result
  bool tos(const LHCb::HltObjectSummary & hos ) override { unsigned int valid; return tos(hos,valid); }

  /// check for TIS  - may be faster than using tisTos()
  bool tis(const LHCb::Particle & particle) override;
  bool tis(const LHCb::RecVertex & recVertex) override;
  bool tis(const LHCb::Vertex & vertex) override;
  bool tis(const LHCb::Track & track ) override;
  bool tis(const std::vector<LHCb::LHCbID> & hits, unsigned int & valid )
    {    return tisSortedHits( sortedHits(hits),valid );  } // output: valid==0 -> no hits, dummy result returned
  bool tis(const std::vector<LHCb::LHCbID> & hits ) override {    return tisSortedHits( sortedHits(hits) );  }
  bool tis(const LHCb::HltObjectSummary & hos, unsigned int & valid ); // output: valid==0 -> no hits, dummy result
  bool tis(const LHCb::HltObjectSummary & hos ) override { unsigned int valid; return tis(hos,valid); }

  /// check for TUS  (TPS or TOS) - may be faster than using tisTos()
  bool tus(const LHCb::Particle & particle) override;
  bool tus(const LHCb::RecVertex & recVertex) override;
  bool tus(const LHCb::Vertex & vertex) override;
  bool tus(const LHCb::Track & track ) override;
  bool tus(const std::vector<LHCb::LHCbID> & hits, unsigned int & valid)
    {    return tusSortedHits( sortedHits(hits),valid );  } // output: valid==0 -> no hits, dummy result returned
  bool tus(const std::vector<LHCb::LHCbID> & hits ) override {    return tusSortedHits( sortedHits(hits) );  }
  bool tus(const LHCb::HltObjectSummary & hos, unsigned int & valid ); // output: valid==0 -> no hits, dummy result
  bool tus(const LHCb::HltObjectSummary & hos ) override { unsigned int valid; return tus(hos,valid); }

  /// analysis string
  std::string analysisReport(const LHCb::Particle & particle) override;
  std::string analysisReport(const LHCb::RecVertex & recVertex) override;
  std::string analysisReport(const LHCb::Vertex & vertex) override;
  std::string analysisReport(const LHCb::Track & track ) override;
  std::string analysisReport(const std::vector<LHCb::LHCbID> & hits ) override { return analysisReportSortedHits( sortedHits(hits) );  }
  std::string analysisReport(const LHCb::HltObjectSummary & hos ) override;

  // --------------------- control calls -------------------------

  void setProjectTracksToCalo(bool onOff) override { m_projectTracksToCalo=onOff;  }
  void setCaloClustForCharged(bool onOff) override { m_caloClustForCharged=onOff;  }
  void setCaloClustForNeutral(bool onOff) override { m_caloClustForNeutral=onOff;  }
  void setCompositeTPSviaPartialTOSonly(bool onOff) override { m_compositeTPSviaPartialTOSonly=onOff;  }
  void setFullAnalysisReport(bool onOff) override { m_fullAnalysisReport=onOff;  }

  bool getProjectTracksToCalo()const override { return m_projectTracksToCalo;  }
  bool getCaloClustForCharged()const override { return m_caloClustForCharged;  }
  bool getCaloClustForNeutral()const override { return m_caloClustForNeutral;  }
  bool getCompositeTPSviaPartialTOSonly()const override { return m_compositeTPSviaPartialTOSonly;  }
  bool getFullAnalysisReport()const override { return m_fullAnalysisReport;  }

  // --------------------- utilities ------------------------

  /// collect hits from protoparticle
  std::vector<LHCb::LHCbID> protoParticleHits(const LHCb::ProtoParticle & pp, bool extend=false );
  /// get calo Hits by projecting into eCal and Hcal (3x3 cells)
  std::vector<LHCb::LHCbID> projectTrack(const LHCb::Track& track );

protected:

  /// Tool for projecting track into Hcal and Ecal
  ITrack2Calo*  m_track2calo;
  /// Hcal detector geometry
  DeCalorimeter* m_hcalDeCal;
  /// Ecal detector geometry
  DeCalorimeter* m_ecalDeCal;

  /// true if signal Tracks are projected to Ecal and Hcal to claim calo cells
  bool m_projectTracksToCalo;
  /// true if signal ProtoParticle built on Track should collect hits from linked CaloCluster
  bool m_caloClustForCharged;
  /// true if signal ProtoParticle built on CaloCluster should collect hits from linked CaloClusters
  bool m_caloClustForNeutral;
  /// true if composite Trigger object should acquire TPS status only if at least one non-composite sub-object is TOS
  bool m_compositeTPSviaPartialTOSonly;

  /// true if full analysis report should be given in analysisReport(), instead of following tisTos() loop breaking
  bool m_fullAnalysisReport;

  /// to offset analysisReport printout
  unsigned int m_reportDepth;

protected:

  std::string offset()
  {
    std::string rep;
    for(unsigned int i=0;i<m_reportDepth;++i){ rep += "   "; }
    return rep;
  }

};
#endif // PARTICLETISTOS_H
