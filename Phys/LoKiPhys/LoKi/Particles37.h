// ============================================================================
#ifndef LOKI_PARTICLES37_H
#define LOKI_PARTICLES37_H 1
// ============================================================================
// Incldue files
// ============================================================================
// Event model
// ============================================================================
#include "Event/RecVertex.h"
#include "Event/Particle.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/PhysTypes.h"
#include "LoKi/Vertices1.h"
#include "LoKi/AuxDesktopBase.h"
#include "LoKi/PhysTypes.h"
#include "LoKi/Particles4.h"
#include "LoKi/Particles20.h"
// ============================================================================
/** @file LoKi/Particles37.h
 *  Collection of functors to calculate the decay length
 *  significance of a particle with respect to a vertex.
 *  The decay length significance is defined as the
 *  projection onto the momentum of a particle of the
 *  distance between the particle and a primary vertex,
 *  divided by the error on the distance.
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Roel Aaij roel.aaij@cern.ch
 *  @date 2010-06-18
 */
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Particles
  {
    // ========================================================================
    /** @class DecayLengthSignificance
     *  Functor which calculates the decay length significance of a
     *  particle with respect to a primary vertex.
     *  @code
     *
     *    LHCb::VertexBase* pv = ...;
     *
     *    // get the DLS functor
     *    const DLS_FUN p1 = DLS( pv );
     *
     *    const LHCb::Particle* B = ...;
     *
     *    const double val = p1 ( B );
     *
     *  @endcode
     *
     *  @author Roel Aaij
     *  @date   2010-06-18
     */
    struct GAUDI_API DecayLengthSignificance
      : LoKi::BasicFunctors< const LHCb::Particle* >::Function
      , LoKi::Vertices::VertexHolder
    {
      // ======================================================================
      /// constructor from a vertex
      DecayLengthSignificance ( const LHCb::VertexBase* vertex );
      /// constructor from a point
      DecayLengthSignificance ( const LoKi::Point3D& vertex );
      /// constructor from VertexHolder
      DecayLengthSignificance ( const LoKi::Vertices::VertexHolder& base );
      /// MANDATORY: clone method ("virtual constructor")
      DecayLengthSignificance* clone() const override;
      /// The method where the actual calculation happens.
      virtual result_type dls( argument p ) const;
      /// MANDATORY: the only essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& stream ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class DecayLengthSignificanceDV
     *  Functor which uses the best primary vertex from the
     *  PhysDesktop to calculate the decay length significance.
     *  @see DecayLengthSignificance
     *
     *  @code
     *
     *    // get the DLS functor
     *    const DLS_FUN fun = DLS();
     *
     *    const LHCb::Particle* B = ...;
     *
     *    const double dls = fun ( B );
     *
     *  @endcode
     *
     *  @author Roel Aaij
     *  @date   2010-05-07
     */
    struct GAUDI_API DecayLengthSignificanceDV
      : DecayLengthSignificance
      , virtual LoKi::AuxDesktopBase
    {
      // ======================================================================
      /// basic constructor
      DecayLengthSignificanceDV();
      /// MANDATORY: clone method ("virtual constructor")
       DecayLengthSignificanceDV* clone() const override
      { return new DecayLengthSignificanceDV( *this ); }
      /// MANDATORY: the only essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "BPVDLS"; }
      // ======================================================================
    };
    // ========================================================================
    /** @class PathDistance
     *  get the path distance
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @see LoKi::Cuts::PATHDIST
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-23
     */
    struct GAUDI_API PathDistance : LoKi::Particles::ImpPar
    {
      // ======================================================================
      /// constructor from the primary vertex & tool
      PathDistance
      ( const LHCb::VertexBase*                        pv     ,
        const IDistanceCalculator*                     tool   ) ;
      /// constructor from the primary vertex & tool
      PathDistance ( const LoKi::Vertices::ImpParBase& tool   ) ;
      /// MANDATORY: clone method ("virtual constructor")
       PathDistance* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class PathDistanceChi2
     *  get the path distance chi2
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::PathDistance
     *  @see LoKi::Cuts::PATHDISTCHI2
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-23
     */
    struct GAUDI_API PathDistanceChi2 : LoKi::Particles::PathDistance
    {
      // ======================================================================
      /// constructor from the primary vertex & tool
      PathDistanceChi2
      ( const LHCb::VertexBase*                        pv     ,
        const IDistanceCalculator*                     tool   ) ;
      /// constructor from the primary vertex & tool
      PathDistanceChi2 ( const LoKi::Vertices::ImpParBase& tool   ) ;
      /// MANDATORY: clone method ("virtual constructor")
       PathDistanceChi2* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class PathDistanceSignificance
     *  get the path distance significnace
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::PathDistance
     *  @see LoKi::Particles::PathDistanceChi2
     *  @see LoKi::Cuts::PATHDISTSIGNIFICANCE
     *  @see LoKi::Cuts::PDS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-23
     */
    struct GAUDI_API PathDistanceSignificance
      : LoKi::Particles::PathDistanceChi2
    {
      // ======================================================================
      /// constructor from the primary vertex & tool
      PathDistanceSignificance
      ( const LHCb::VertexBase*                        pv     ,
        const IDistanceCalculator*                     tool   ) ;
      /// constructor from the primary vertex & tool
      PathDistanceSignificance ( const LoKi::Vertices::ImpParBase& tool   ) ;
      /// MANDATORY: clone method ("virtual constructor")
       PathDistanceSignificance* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class ProjDistance
     *  get the ''projected-distance''
     *  @see IDistanceCalculator::projectedDistance
     *  @see LoKi::Cuts::PROJDIST
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-23
     */
    struct GAUDI_API ProjectedDistance : LoKi::Particles::PathDistance
    {
      // ======================================================================
      /// constructor from the primary vertex & tool
      ProjectedDistance
      ( const LHCb::VertexBase*                        pv     ,
        const IDistanceCalculator*                     tool   ) ;
      /// constructor from the primary vertex & tool
      ProjectedDistance ( const LoKi::Vertices::ImpParBase& tool   ) ;
      /// MANDATORY: clone method ("virtual constructor")
       ProjectedDistance* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class ProjectedDistanceSignificance
     *  get the ''projected-distance'' significance
     *  @see IDistanceCalculator::projectedDistance
     *  @see LoKi::Cuts::PROJDS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-23
     */
    struct GAUDI_API ProjectedDistanceSignificance
      : LoKi::Particles::ProjectedDistance
    {
      // ======================================================================
      /// constructor from the primary vertex & tool
      ProjectedDistanceSignificance
      ( const LHCb::VertexBase*                        pv     ,
        const IDistanceCalculator*                     tool   ) ;
      /// constructor from the primary vertex & tool
      ProjectedDistanceSignificance ( const LoKi::Vertices::ImpParBase& tool   ) ;
      /// MANDATORY: clone method ("virtual constructor")
       ProjectedDistanceSignificance* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class PathDistanceWithBestPV
     *  get the path distance
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @see LoKi::Cuts::BPVPATHDIST
     *  @see LoKi::Cuts::PATHDIST
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-23
     */
    struct GAUDI_API PathDistanceWithBestPV
      : LoKi::Particles::ImpParWithTheBestPV
    {
      // ======================================================================
      /// constructor from tool nickname
      PathDistanceWithBestPV ( const std::string& geo = "" )  ;
      /// MANDATORY: clone method ("virtual constructor")
       PathDistanceWithBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class PathDistanceChi2WithBestPV
     *  get the path distance
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @see LoKi::Cuts::BPVPATHDISTCHI2
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-23
     */
    struct PathDistanceChi2WithBestPV : LoKi::Particles::PathDistanceWithBestPV
    {
      // ======================================================================
      /// constructor from tool nickname
      PathDistanceChi2WithBestPV ( const std::string& geo = "" )  ;
      /// MANDATORY: clone method ("virtual constructor")
       PathDistanceChi2WithBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class PathDistanceSignificanceWithBestPV
     *  get the path distance
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @see LoKi::Cuts::BPVPDS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-23
     */
    struct GAUDI_API PathDistanceSignificanceWithBestPV
      : LoKi::Particles::PathDistanceChi2WithBestPV
    {
      // ======================================================================
      /// constructor from tool nickname
      PathDistanceSignificanceWithBestPV ( const std::string& geo = "" )  ;
      /// MANDATORY: clone method ("virtual constructor")
       PathDistanceSignificanceWithBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class ProjectedDistanceWithBestPV
     *  get the path distance
     *  @see IDistanceCalculator::projectedDistance
     *  @see LoKi::Cuts::BPVPROJDIST
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-23
     */
    struct GAUDI_API ProjectedDistanceWithBestPV
      : LoKi::Particles::PathDistanceWithBestPV
    {
      // ======================================================================
      /// constructor from tool nickname
      ProjectedDistanceWithBestPV ( const std::string& geo = "" )  ;
      /// MANDATORY: clone method ("virtual constructor")
       ProjectedDistanceWithBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class ProjectedDistanceSignificanceWithBestPV
     *  get the path distance
     *  @see IDistanceCalculator::projectedDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @see LoKi::Cuts::BPVPROJDS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-10-23
     */
    struct GAUDI_API ProjectedDistanceSignificanceWithBestPV
      :  LoKi::Particles::ProjectedDistanceWithBestPV
    {
      // ======================================================================
      /// constructor from tool nickname
      ProjectedDistanceSignificanceWithBestPV ( const std::string& geo = "" )  ;
      /// MANDATORY: clone method ("virtual constructor")
       ProjectedDistanceSignificanceWithBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
  } //                                        end of namespace LoKi:: Particles
  // ==========================================================================
  namespace Cuts
  {
    // ========================================================================
    /** @typedef DLS
     *  Functor which calculates the decay length significance of a
     *  particle with respect to a primary vertex.
     *  @see LoKi::Particles::DecayLengthSignificance
     *
     *  @code
     *
     *    const LHCb::VertexBase* pv = ...;
     *
     *    // get the DLS functor
     *    const DLS p1 = DLS( pv );
     *
     *    const LHCb::Particle* B = ...;
     *
     *    const double val = p1 ( B );
     *
     *  @endcode
     *
     *  @author Roel Aaij
     *  @date   2010-06-18
     */
    typedef LoKi::Particles::DecayLengthSignificance                      DLS ;
    // ========================================================================
    /** @typedef BPVDLS
     *  Functor which uses the best primary vertex from the
     *  PhysDesktop to calculate the decay length significance.
     *  @see LoKi::Particles::DecayLengthSignificance
     *  @see LoKi::Particles::DecayLengthSignificanceDV
     *
     *  @author Roel Aaij
     *  @date   2010-05-07
     */
    typedef LoKi::Particles::DecayLengthSignificanceDV                 BPVDLS ;
    // ========================================================================
    /** @typedef PATHDIST
     *  Functor which calculates the 'path-distance'
     *  particle with respect to a primary vertex.
     *
     *  @code
     *
     *    const LHCb::VertexBase* pv = ...;
     *
     *    // get the PATHDIST functor
     *    const PATHDIST  fun = PATHDIST( pv );
     *
     *    const LHCb::Particle* B = ...;
     *
     *    const double distance = fun ( B );
     *
     *  @endcode
     *
     *  @see LoKi::Particles::PathDistance
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */
    typedef LoKi::Particles::PathDistance                            PATHDIST ;
    // ========================================================================
    /** @typedef PATHDISTCHI2
     *  Functor which calculates the chi2 for 'path-distance'
     *  particle with respect to a primary vertex.
     *
     *  @code
     *
     *    const LHCb::VertexBase* pv = ...;
     *
     *    // get the PATHDISTCHI2 functor
     *    const PATHDISTCHI2  fun = PATHDISTCHI2 ( pv );
     *
     *    const LHCb::Particle* B = ...;
     *
     *    const double chi2 = fun ( B );
     *
     *  @endcode
     *
     *  @see LoKi::Particles::PathDistanceChi2
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @see LoKi::Cuts::PATHDIST
     *  @see LoKi::Cuts::PATHDISTSIGNIFICANCE
     *  @see LoKi::Cuts::PDS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */
    typedef LoKi::Particles::PathDistanceChi2                    PATHDISTCHI2 ;
    // ========================================================================
    /** @typedef PATHDISTSIGNIFICANCE
     *  Functor which calculates the significance for 'path-distance'
     *  particle with respect to a primary vertex.
     *
     *  @code
     *
     *    const LHCb::VertexBase* pv = ...;
     *
     *    // get the functor
     *    const PATHDISTSIGNIFICANCE  fun = PATHDISTSIGNIFICANCE ( pv );
     *
     *    const LHCb::Particle* B = ...;
     *
     *    const double chi2 = fun ( B );
     *
     *  @endcode
     *
     *  @see LoKi::Particles::PathDistanceSignificance
     *  @see LoKi::Particles::PathDistanceChi2
     *  @see LoKi::Particles::PathDistance
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @see LoKi::Cuts::PATHDIST
     *  @see LoKi::Cuts::PATHDISTCHI2
     *  @see LoKi::Cuts::PDS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */
    typedef LoKi::Particles::PathDistanceSignificance    PATHDISTSIGNIFICANCE ;
    // ========================================================================
    /** @typedef PDS
     *  Functor which calculates the significance for 'path-distance'
     *  particle with respect to a primary vertex.
     *
     *  @code
     *
     *    const LHCb::VertexBase* pv = ...;
     *
     *    // get the functor
     *    const PDS  fun = PDS ( pv );
     *
     *    const LHCb::Particle* B = ...;
     *
     *    const double chi2 = fun ( B );
     *
     *  @endcode
     *
     *  @see LoKi::Particles::PathDistanceSignificance
     *  @see LoKi::Particles::PathDistanceChi2
     *  @see LoKi::Particles::PathDistance
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @see LoKi::Cuts::PATHDIST
     *  @see LoKi::Cuts::PATHDISTCHI2
     *  @see LoKi::Cuts::PATHDISTANCESIGNIFICANCE
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */
    typedef LoKi::Particles::PathDistanceSignificance                     PDS ;
    // ========================================================================
    /** @typedef PROJDIST
     *  Functor which calculates the 'projected-distance'
     *  particle with respect to a primary vertex.
     *
     *  @code
     *
     *    const LHCb::VertexBase* pv = ...;
     *
     *    // get the functor
     *    const PROJDIST  fun = PROJDIST( pv );
     *
     *    const LHCb::Particle* B = ...;
     *
     *    const double distance = fun ( B );
     *
     *  @endcode
     *
     *  @see LoKi::Particles::ProjectedDistance
     *  @see IDistanceCalculator::projectedDistance
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */
    typedef LoKi::Particles::ProjectedDistance                       PROJDIST ;
    // ========================================================================
    /** @typedef PROJDS
     *  Functor which calculates the 'projected-distance' significance
     *  particle with respect to a primary vertex.
     *
     *  @code
     *
     *    const LHCb::VertexBase* pv = ...;
     *
     *    // get the functor
     *    const PROJDS  fun = PROJDS ( pv );
     *
     *    const LHCb::Particle* B = ...;
     *
     *    const double distance = fun ( B );
     *
     *  @endcode
     *
     *  @see LoKi::Particles::ProjectedDistanceSignificance
     *  @see IDistanceCalculator::projectedDistance
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */
    typedef LoKi::Particles::ProjectedDistanceSignificance             PROJDS ;
    // ========================================================================
    /** @typedef BPVPATHDIST_
     *  Functor which uses the best primary vertex from the
     *  PhysDesktop to calculate the decay length significance.

     *
     *  @see LoKi::Particles::PathDistanceWithBestPV
     *  @see LoKi::Particles::PathDistance
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @see LoKi::Cuts::PATHDIST
     *  @see LoKi::Cuts::BPVPATHDIST
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */
    typedef LoKi::Particles::PathDistanceWithBestPV              BPVPATHDIST ;
    // ========================================================================
    /** @typedef BPVPATHDISTCHI2
     *  Functor which uses the best primary vertex from the
     *  PhysDesktop to calculate the decay length significance.
     *
     *  @see LoKi::Particles::PathDistanceChi2WithBestPV
     *  @see LoKi::Particles::PathDistanceChi2
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @see LoKi::Cuts::PATHDIST
     *  @see LoKi::Cuts::BPVPATHDIST
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */
    typedef LoKi::Particles::PathDistanceChi2WithBestPV      BPVPATHDISTCHI2 ;
    // ========================================================================
    /** @typedef BPVPDS
     *  Functor which uses the best primary vertex from the
     *  PhysDesktop to calculate the decay length significance.
     *
     *  @see LoKi::Particles::PathDistanceSignificanceWithBestPV
     *  @see LoKi::Particles::PathDistanceSignificance
     *  @see IDistanceCalculator::pathDistance
     *  @see LoKi::Particles::ImpPar::path
     *  @see LoKi::Cuts::PATHDIST
     *  @see LoKi::Cuts::BPVPATHDIST
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */
    typedef LoKi::Particles::PathDistanceSignificanceWithBestPV        BPVPDS ;
    // ========================================================================
    /** @typedef BPVPROJDIST_
     *  Functor which uses the best primary vertex from the
     *  PhysDesktop to calculate the projected disatnce
     *
     *  @see LoKi::Particles::ProjectedDistanceWithBestPV
     *  @see IDistanceCalculator::projectedDistance
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */
    typedef LoKi::Particles::ProjectedDistanceWithBestPV         BPVPROJDIST  ;
    // ========================================================================
    /** @typedef BPVPROJDS
     *  Functor which uses the best primary vertex from the
     *  PhysDesktop to calculate the significance of projected distance
     *
     *  @see LoKi::Particles::ProjectedDistanceWithBestPV
     *  @see IDistanceCalculator::projectedDistance
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */
    typedef LoKi::Particles::ProjectedDistanceSignificanceWithBestPV BPVPROJDS ;
    // ========================================================================
  } //                                              end of namespace LoKi::Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // FUNCTORS_H
// ============================================================================
