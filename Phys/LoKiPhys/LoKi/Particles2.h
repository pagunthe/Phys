// ============================================================================
#ifndef LOKI_PARTICLES2_H
#define LOKI_PARTICLES2_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/PhysTypes.h"
#include "LoKi/Particles1.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-19
 */
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Particles
  {
    // ========================================================================
    /** @class TimeDistance
     *
     *  evaluator of the time distance (c*tau)
     *  between particle vertex and the 'vertex'
     *
     *  LoKi::Particles::VertexDistance is used
     *  for evaluation of distance
     *
     *  @see LoKi::Particles::VertexDistance
     *  @see LHCb::Particle
     *  @see LHCb::Vertex
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2002-07-15
     */
    class GAUDI_API TimeDistance : public LoKi::Particles::VertexDistance
    {
    public:
      // ======================================================================
      /// constructor
      TimeDistance
      ( const LHCb::VertexBase*  vertex )
        : LoKi::Particles::VertexDistance ( vertex ) {}
      /// constructor
      TimeDistance ( const LoKi::Point3D&     vertex ) ;
      /// constructor
      TimeDistance
        ( const LoKi::Vertices::VertexHolder& vertex )
        : LoKi::Particles::VertexDistance ( vertex ) {}
      /// MANDATORY: clone method ("virtual constructor")
       TimeDistance* clone() const override
      { return new LoKi::Particles::TimeDistance(*this) ; }
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override
      { return time ( p ) ; }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TDIST" ; }
      /// the actual computation
      result_type  time  ( argument p ) const ;
      // ======================================================================
    private:
      // ======================================================================
      /// default constructor is private
      TimeDistance();
      // ======================================================================
    };
    // ========================================================================
    /** @class TimeSignedDistance
     *
     *  evaluator of the time distance (c*tau)
     *  between particle vertex and the 'vertex'
     *
     *  LoKi::Particles::VertexSignedDistance is used
     *  for evaluation of distance
     *
     *  @see LoKi::Particles::VertexSignedDistance
     *  @see LHCb::Particle
     *  @see LHCb::Vertex
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2002-07-15
     */
    class GAUDI_API TimeSignedDistance : public LoKi::Particles::VertexSignedDistance
    {
    public:
      // ======================================================================
      /// constructor
      TimeSignedDistance
      ( const LHCb::VertexBase*  vertex )
        : LoKi::Particles::VertexSignedDistance ( vertex ) {}
      /// constructor
      TimeSignedDistance ( const LoKi::Point3D&     vertex ) ;
      /// constructor
      TimeSignedDistance
      ( const LoKi::Vertices::VertexHolder& vertex )
        : LoKi::Particles::VertexSignedDistance ( vertex ) {}
      /// MANDATORY: clone method ("virtual constructor")
       TimeSignedDistance* clone() const override
      { return new TimeSignedDistance ( *this ) ; }
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override
      { return time ( p ) ; }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TDSIGN" ; }
      /// the actual computation
      result_type  time  ( argument p ) const ;
      // ======================================================================
    private:
      // ======================================================================
      /// default constructor is private
      TimeSignedDistance();
      // ======================================================================
    };
    // ========================================================================
    /** @class TimeDotDistance
     *
     *  evaluator of the time distance (c*tau)
     *  between particle vertex and the 'vertex'
     *
     *  LoKi::Particles::VertexDotDistance is used
     *  for evaluation of distance
     *
     *  @see LoKi::Particles::VertexDotDistance
     *  @see LHCb::Particle
     *  @see LHCb::Vertex
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2002-07-15
     */
    class GAUDI_API TimeDotDistance : public LoKi::Particles::VertexDotDistance
    {
    public:
      // ======================================================================
      /// constructor
      TimeDotDistance
      ( const LHCb::VertexBase* vertex )
        : LoKi::Particles::VertexDotDistance ( vertex ) {}
      /// constructor
      TimeDotDistance ( const LoKi::Point3D& vertex ) ;
      /// constructor
      TimeDotDistance
      ( const LoKi::Vertices::VertexHolder&    vertex )
        : LoKi::Particles::VertexDotDistance ( vertex ) {}
      /// MANDATORY: virtual destructor
       TimeDotDistance* clone() const override
      { return new TimeDotDistance(*this) ; }
      /// MANDATORY: the only one essential method
      result_type operator() ( argument p ) const override
      { return time ( p ) ; }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TDOT" ; }
      /// the actual computation
      result_type time           ( argument p ) const ;
      // ======================================================================
    private:
      // ======================================================================
      /// default constructor is private
      TimeDotDistance();
      // ======================================================================
    };
    // ========================================================================
    /** @class LifetimeDistance
     *
     *  The estimate of the particle lifetime (c*tau), based on the
     *  first iteration of "Lifetime-fitter" and neglecting the errors
     *   in particle momenta
     *
     *  Essentially the algorithm is described in detail by Paul Avery in
     *     http://www.phys.ufl.edu/~avery/fitting/lifetime.pdf
     *
     *  More precise variants (more iterations) use
     *   the abstract interface ILifetimeFitter
     *
     *  @see LoKi::Cuts::LT
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-19
     */
    class GAUDI_API LifetimeDistance
      : public LoKi::BasicFunctors<const LHCb::Particle*>::Function
      , public LoKi::Vertices::VertexHolder
    {
    public:
      // ======================================================================
      /// constructor
      LifetimeDistance
      ( const LHCb::VertexBase* vertex ) ;
      /// constructor
      LifetimeDistance
      ( const LoKi::Point3D&    vertex ) ;
      /// constructor
      LifetimeDistance
      ( const LoKi::Vertices::VertexHolder&       base  ) ;
      /// MANDATORY: clone method ("virtual constructor")
       LifetimeDistance* clone() const override;
      /// MANDATORY: the only one essential method
       result_type operator() ( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// The default constructor is disabled
      LifetimeDistance() ; // No default constrtuctor
      // ======================================================================
    } ;
    // ========================================================================
  } //                                         end of namespace LoKi::Particles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES1_H
// ============================================================================
