#ifndef RECOTRACK_H
#define RECOTRACK_H

#include "GaudiKernel/Vector3DTypes.h"
#include "RecoParticle.h"
#include "Configuration.h"
#include "Event/State.h"

struct ITrackStateProvider ;
namespace LHCb {
  class TrackTraj ;
  class Track ;
}

namespace DecayTreeFitter
{

  class RecoTrack : public RecoParticle
  {
  public:
    RecoTrack(const LHCb::Particle& bc, const ParticleBase* mother,
	      const Configuration& config) ;
    virtual ~RecoTrack() ;

    ErrCode initPar2(FitParams*) override;
    ErrCode initCov(FitParams*) const override;
    int dimM() const override { return 5 ; }
    int type() const override { return kRecoTrack ; }

    ErrCode projectRecoConstraint(const FitParams&, Projection&) const override;
    ErrCode updCache(const FitParams& fitparams) ;
    //tatic void setApplyCovCorrection(bool b=true) { gApplyCovCorrection = b ; }
    //static void correctCov(HepSymMatrix& V) ;

    int nFinalChargedCandidates() const override { return 1 ; }

    void addToConstraintList(constraintlist& alist, int depth) const override {
      alist.push_back(Constraint(this,Constraint::track,depth,dimM()) ) ;
    }
    //ErrCode updFltToMother(const FitParams& fitparams) ;
    void setFlightLength(double flt) { m_flt = flt ; }
    const LHCb::Track& track() const { return *m_track ; }
    const LHCb::State& state() const { return m_state ; }

    // return a trajectory (declared by base class)
    const LHCb::Trajectory<double>* trajectory() const  override;

    // return a tracktraj
    const LHCb::TrackTraj* tracktraj() const ;

  private:
    const Gaudi::XYZVector m_bfield ;
    const LHCb::Track* m_track ;
    const ITrackStateProvider* m_stateprovider ;
    bool m_useTrackTraj ;
    const LHCb::TrackTraj* m_tracktraj ;
    bool m_ownstracktraj ;
    bool m_cached ;
    double m_flt ;
    LHCb::State m_state ;
    double m_bremEnergy ;
    double m_bremEnergyCov ;
  } ;

}
#endif
