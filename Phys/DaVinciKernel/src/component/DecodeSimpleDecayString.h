// $Id$
// ============================================================================
#ifndef DECODESIMPLEDECAYSTRING_H
#define DECODESIMPLEDECAYSTRING_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/AlgTool.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// DaVinciKernel
// ============================================================================
#include "Kernel/Decay.h"
#include "Kernel/IDecodeSimpleDecayString.h"            // Interface
// ============================================================================
// forwarde declaration
// ============================================================================
namespace LHCb { class IParticlePropertySvc; }
// ============================================================================
/** @class DecodeSimpleDecayString DecodeSimpleDecayString.h
 *
 *
 *  @author Patrick KOPPENBURG
 *  @date   2004-06-30
 */
class DecodeSimpleDecayString : public GaudiTool,
                                virtual public IDecodeSimpleDecayString
{

public:

  /// Standard constructor
  DecodeSimpleDecayString( const std::string& type,
                           const std::string& name,
                           const IInterface* parent);

  ~DecodeSimpleDecayString( ); ///< Destructor

  /// Initialize method
  StatusCode initialize() override;

public:

  StatusCode setDescriptor(const std::string&) override;
  std::string getDescriptor() const override {return m_descriptor;} ;
  StatusCode getStrings(std::string&,
                        std::vector<std::string>&) const override;
  StatusCode getStrings_cc
  (std::string&, std::vector<std::string>&) const override;
  StatusCode getPIDs(int&, std::vector<int>&) const override;
  StatusCode getPIDs_cc(int&, std::vector<int>&) const override;
  bool is_cc(void) const override;

public:

  // ==========================================================================
  /** get the decay form the descriptor
   *  @param decay (output) the decay
   *  @return status code
   */
  StatusCode getDecay
  ( Decays::Decay& decay ) const  override;
  // ==========================================================================
  /** get the charge conjugated decay form the descriptor
   *  @param decay (output) the decay
   *  @return status code
   */
  StatusCode getDecay_cc
  ( Decays::Decay& decay ) const override;
  // ==========================================================================
  /** get all decays form the descriptor
   *  @param decays (output) the vector of decays
   *  @return status code
   */
  StatusCode getDecays
  ( std::vector<Decays::Decay>& decays ) const override;
  // ==========================================================================

private:

  StatusCode reset();
  StatusCode PID(const std::string&, int&) const;
  StatusCode splitDescriptor(const std::string&,std::string&,
                             std::vector<std::string>&) const;
  StatusCode strip_cc(void) ;
  StatusCode buildPIDs(const std::string&,
                       const std::vector<std::string>&,
                       int&,
                       std::vector<int>&) const;
  StatusCode do_cc(void) ;
  std::string conjugate(const std::string&)const;

private:

  std::string m_descriptor;
  std::string m_mother;
  std::vector<std::string> m_daughters;
  std::string m_mother_cc;
  std::vector<std::string> m_daughters_cc;
  LHCb::IParticlePropertySvc* m_ppSvc;
  bool m_iscc;

};
// =========================================================================
#endif // DECODESIMPLEDECAYSTRING_H
