################################################################################
# Package: DaVinciPVTools
################################################################################
gaudi_subdir(DaVinciPVTools)

gaudi_depends_on_subdirs(Phys/DaVinciKernel
                         Event/MicroDst)

find_package(ROOT)
find_package(Boost)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

gaudi_add_module(DaVinciPVTools
                 src/*.cpp
                 LINK_LIBRARIES DaVinciKernelLib MicroDstLib)

gaudi_install_python_modules()
